#include <stdio.h>
#include "fun.h"

int main (int argc, const char* argv[])
{
	printf("Let\'s try some calculations\n");
    printf(" 2 * 4 = %d\n", mul(2, 4));
    printf(" 5 + 6 = %d\n", add(5, 6));

	return 0;
}