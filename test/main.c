#include <embUnit/embUnit.h>
#include <stdio.h>
#include "fun.h"


TestRef tests(void);

int main (int argc, const char* argv[])
{
	TestRunner_start();
		TestRunner_runTest(tests());
	TestRunner_end();
	return 0;
}