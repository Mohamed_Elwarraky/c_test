#include <embUnit/embUnit.h>
#include "fun.h"

static void testMul(void)
{
	TEST_ASSERT_EQUAL_INT(6, mul(2,3));
}

static void testAdd(void)
{
	TEST_ASSERT_EQUAL_INT(5, add(3,2));

}

TestRef tests(void)
{
	EMB_UNIT_TESTFIXTURES(fixtures) {
		new_TestFixture("MUL",testMul),
		new_TestFixture("ADD",testAdd),
	};
	EMB_UNIT_TESTCALLER(test,"Test",add, mul, fixtures);

	return (TestRef)&test;
}