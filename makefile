all:
	-@mkdir lib
	-@cd ./embUnit		;$(MAKE)
	-@cd ./tools		;$(MAKE)
	-@cd ./test   		;$(MAKE)

test:
	-@./src/src

clean:
	-@cd ./embUnit		;$(MAKE) clean
	-@cd ./tools		;$(MAKE) clean
	-@cd ./test  		;$(MAKE) clean

.PHONY: clean test all
